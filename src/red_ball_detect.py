#!/usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import rospy
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from geometry_msgs.msg import Pose2D
import time
import numpy as np

np.set_printoptions(threshold=10000,suppress=False,precision=2)

class RedBallDetect():
    def __init__(self):
        rospy.Subscriber('/cam_image/3/scene', Image, self.handle_image) # downward camera
        self.cvbridge = CvBridge()
        self.image = None

        self.receive_time = 0
        self.TimeThresh = 5

        self.color_min = np.array([20,20,180])
        self.color_max = np.array([150,150,255])

    def handle_image(self, msg):
        self.receive_time = time.time()
        self.image = self.cvbridge.imgmsg_to_cv2(msg, "bgr8")

    def detect(self):
        if self.image is not None and time.time()-self.receive_time < self.TimeThresh:
            # detection using color
            mask = cv2.inRange(self.image, self.color_min, self.color_max)
            output = cv2.bitwise_and(self.image,self.image, mask= mask)
            # Detect blobs, calculate the center
            pt_num, cum_x, cum_y = 0, 0, 0
            for w in range(mask.shape[0]):
                for s in range(mask.shape[1]):
                    if mask[w][s]>0.5:
                        pt_num += 1
                        cum_y += w
                        cum_x += s
            cv2.imshow('img',np.concatenate((self.image,output),axis=1))
            cv2.waitKey(1)
            if pt_num>0:
                return (cum_x/pt_num-mask.shape[1]/2, cum_y/pt_num-mask.shape[0]/2)
            else: # no target detected
                return (0,0)
             
        else: # no image received
            return (0,0)


if __name__ == '__main__':

    rospy.init_node('red_ball_detect', anonymous=True)
    rate = rospy.Rate(10)
    posePub = rospy.Publisher('red_ball_detect/ball_pose',Pose2D,queue_size=10) # publish the center of the target

    readball = RedBallDetect()
    while not rospy.is_shutdown():

        direction = readball.detect()
        # rospy.info("derection",direction)

        ballpose = Pose2D()
        ballpose.x = direction[0]
        ballpose.y = direction[1]

        posePub.publish(ballpose)

        rate.sleep()
