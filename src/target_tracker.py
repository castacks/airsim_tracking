#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from geometry_msgs.msg import Pose2D, Twist


class TargetTracker():
    def __init__(self):
        rospy.Subscriber('/red_ball_detect/ball_pose', Pose2D, self.handle_control) # downward camera
        self.velPub = rospy.Publisher('/airsim/cmd_vel',Twist,queue_size=10)
        self.p = 0.005


    def handle_control(self, msg):
        xerror = msg.x
        yerror = msg.y
        # print xerror, yerror
        cmd_vel = Twist()
        cmd_vel.linear.x = self.p * yerror *-1  # coordinate difference
        cmd_vel.linear.y = self.p * xerror
        self.velPub.publish(cmd_vel) 


if __name__ == '__main__':

    rospy.init_node('target_tracker', anonymous=True)
    rate = rospy.Rate(10)
    posePub = rospy.Publisher('red_ball_detect/ball_pose',Pose2D,queue_size=10) # publish the center of the target

    targetTracker = TargetTracker()

    rospy.spin()
    # while not rospy.is_shutdown():

    #     rate.sleep()
