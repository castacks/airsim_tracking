#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy 
from airsim_adapter.srv import *

if __name__ == '__main__':

	rospy.init_node('takeoff', anonymous=True)

	rospy.wait_for_service('/airsim/takeoff')

	try:
		takeoff = rospy.ServiceProxy('/airsim/takeoff', takeoff)
		takeoff(True)

	except rospy.ServiceException, e:
		print "Service call failed: %s"%e